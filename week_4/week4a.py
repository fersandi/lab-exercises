import week4

def main():
	eraser = week4.make_item("eraser", 1.00)
	coin = week4.make_item("coin", 0.50)
	receipt = week4.make_item("receipt", 0)

	pencilCase = week4.make_composite("Pencil Case", eraser, coin, receipt)

	book = week4.make_item("Python in practice", 39.99)
	phone = week4.make_item("Nokia 3310", 59.99)

	box = week4.make_composite("Box", pencilCase, book, phone)

if __name__ == "__main__":
    main()